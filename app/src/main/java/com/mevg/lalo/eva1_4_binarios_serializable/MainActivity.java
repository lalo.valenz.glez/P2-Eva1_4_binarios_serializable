package com.mevg.lalo.eva1_4_binarios_serializable;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class MainActivity extends AppCompatActivity {
    EditText txtnNombre, txtApellido ,txtEdad;
    RadioButton rbH, rbM;
    Button btnGuardar, btnAbrir;
    TextView txvlectura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtnNombre = (EditText) findViewById(R.id.nombre);
        txtApellido = (EditText) findViewById(R.id.apellido);
        txtEdad = (EditText) findViewById(R.id.edad);
        btnGuardar = (Button) findViewById(R.id.guardar);
         btnAbrir= (Button) findViewById(R.id.abrir);
        rbH = (RadioButton) findViewById(R.id.hombre);
        rbM = (RadioButton) findViewById(R.id.mujer);
        txvlectura = (TextView) findViewById(R.id.lectura);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Datos obj = new Datos();
                obj.setNombre(txtnNombre.getText().toString());
                obj.setApellido(txtApellido.getText().toString());
                obj.setEdad(Integer.parseInt(txtEdad.getText().toString()));
                obj.setSexo(rbH.isChecked());

                try {
                    FileOutputStream arch = openFileOutput("datos.me", MODE_PRIVATE);
                    ObjectOutputStream guardar = new ObjectOutputStream(arch);
                    guardar.writeObject(obj);
                    guardar.close();
                    Toast.makeText(MainActivity.this, "Datos Guardados! ", Toast.LENGTH_SHORT).show();
                    txtnNombre.setText("");
                    txtApellido.setText("");
                    txtEdad.setText("");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                }

            }
        });

        btnAbrir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileInputStream fis = openFileInput("datos.me");
                    ObjectInputStream lectura = new ObjectInputStream(fis);
                    Datos obj;// = (Datos) lectura.readObject();
                    //lectura.r
                    while((obj = (Datos) lectura.readObject()) != null){
                        txvlectura.append("Nombre: " + obj.getNombre() + "\n");
                        txvlectura.append("Apellido: " + obj.getApellido() + "\n");
                        txvlectura.append("Edad: " + obj.getEdad() + "\n");
                        txvlectura.append("Sexo: " + (obj.getSexo() ? "Hombre \n" : "Mujer \n"));
                        obj = (Datos) lectura.readObject();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    public void borraData(View v){
        if(v instanceof EditText){
            EditText some = (EditText) v;
            some.setText("");
        }
    }
}

class Datos implements Serializable{
    private String nombre;
    private String apellido;
    private int edad;
    private boolean sexo;

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setApellido(String apellido){
        this.apellido = apellido;
    }

    public void setEdad(int edad){
        this.edad = edad;
    }

    public void setSexo(boolean sexo){
        this.sexo = sexo;
    }

    public String getNombre(){
        return this.nombre;
    }

    public String getApellido(){
        return this.apellido;
    }

    public int getEdad(){
        return this.edad;
    }

    public boolean getSexo(){
        return this.sexo;
    }
}
